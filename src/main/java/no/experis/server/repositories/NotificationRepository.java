package no.experis.server.repositories;

import no.experis.server.models.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
    Set<Notification> findAllByReceiverUserIdAndSeenIsFalse(Long id);
}
