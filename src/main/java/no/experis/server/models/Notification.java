package no.experis.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name="notification")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="message")
    private String message;

    @Column(name="seen")
    private boolean seen;

    @ManyToOne(optional = false)
    @JoinColumn(name = "sender_user_id", referencedColumnName = "id")
    public User senderUser;

    @ManyToOne(optional = false)
    @JoinColumn(name = "receiver_user_id", referencedColumnName = "id")
    public User receiverUser;



    @JsonGetter("senderUser")
    public String senderUserGetter() {
        if(senderUser != null) {
            return "/api/v1/users/" + senderUser.getId();
        }
        return null;
    }

    @JsonGetter("receiverUser")
    public String receiverUserGetter() {
        if(receiverUser != null) {
            return "/api/v1/user/" + receiverUser.getId();
        }
        return null;
    }

    public Notification() { }

    public Notification(Long id, String message, boolean seen, User senderUser, User receiverUser) {
        this.id = id;
        this.message = message;
        this.seen = seen;
        this.senderUser = senderUser;
        this.receiverUser = receiverUser;

    }

    public Long getId() { return id; }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSenderUser(User senderUser) {
        this.senderUser = senderUser;
    }

    public void setReceiverUser(User receiverUser) {
        this.receiverUser = receiverUser;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

}
