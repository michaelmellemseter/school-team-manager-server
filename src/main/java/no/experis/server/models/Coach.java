package no.experis.server.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="coach")
public class Coach{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @ManyToMany
    @JoinTable(
            name="coach_team",
            joinColumns={@JoinColumn(name="coach_id")},
            inverseJoinColumns={@JoinColumn(name="team_id")}
    )
    public Set<Team> teams;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="user_id")
    public User user;

    public Coach(Long id, Set<Team> teams, User user) {
        this.id = id;
        this.teams = teams;
        this.user = user;
    }

    public Coach(){ }

    public Long getId() { return id; }

    public void setUser(User user) { this.user = user; }

    public User getUser() { return user; }

    public Set<Team> getTeams() { return teams; }

    public void setTeams(Set<Team> teams) { this.teams = teams; }
}
