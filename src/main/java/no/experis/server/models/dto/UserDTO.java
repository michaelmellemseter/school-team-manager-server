package no.experis.server.models.dto;


public class UserDTO {
    public String email;
    public String firstName;
    public String surName;
    public Long roleId;
}
