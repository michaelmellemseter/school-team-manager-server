package no.experis.server.models.dto;

import no.experis.server.models.Player;

public class PlayerDTO {
    public Player player;
    public int jerseyNr;

    public PlayerDTO(Player player, int jerseyNr) {
        this.player = player;
        this.jerseyNr = jerseyNr;
    }
}
