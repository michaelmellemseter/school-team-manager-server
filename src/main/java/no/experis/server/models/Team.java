package no.experis.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "team")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "school_id")
    public School school;

    @JsonGetter("school")
    public String schoolGetter() {
        if(school != null) {
            return "/api/v1/schools/" + school.getId();
        }
        return null;
    }

    @ManyToOne
    @JoinColumn(name = "sport_id")
    public Sport sport;

    @ManyToOne
    @JoinColumn(name = "location_id")
    public Location location;

    @OneToMany(mappedBy = "homeTeam")
    public Set<Match> homeMatches;

    @JsonGetter("homeMatches")
    public List<String> homeMatchesGetter() {
        if(homeMatches != null) {
            return homeMatches.stream()
                    .map(homeMatch -> {
                        return "/api/v1/homeMatches/" + homeMatch.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    @OneToMany(mappedBy = "awayTeam")
    public Set<Match> awayMatches;

    @JsonGetter("awayMatches")
    public List<String> awayMatchesGetter() {
        if(awayMatches != null) {
            return awayMatches.stream()
                    .map(awayMatch -> {
                        return "/api/v1/awayMatches/" + awayMatch.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    @ManyToMany
    @JoinTable(
            name="coach_team",
            joinColumns={@JoinColumn(name="team_id")},
            inverseJoinColumns={@JoinColumn(name="coach_id")}
    )
    public Set<Coach> coaches;

    @JsonGetter("coaches")
    public List<String> coachesGetter() {
        if(coaches != null) {
            return coaches.stream()
                    .map(coach -> {
                        return "/api/v1/coaches/" + coach.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    @OneToMany(mappedBy = "team", cascade = CascadeType.REMOVE)
    private Set<TeamPlayer> teamPlayers;

    /*
    @JsonGetter("players")
    public List<String> playersGetter() {
        if(players != null) {
            return players.stream()
                    .map(player -> {
                        return "/api/v1/players/" + player.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }
       */
    public Team() {

    }

    public Team(Long id, String name, School school, Sport sport, Location location, Set<Match> homeMatches, Set<Match> awayMatches, Set<Coach> coaches, Set<TeamPlayer> teamPlayers) {
        this.id = id;
        this.name = name;
        this.school = school;
        this.sport = sport;
        this.location = location;
        this.homeMatches = homeMatches;
        this.awayMatches = awayMatches;
        this.coaches = coaches;
       this.teamPlayers = teamPlayers;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<TeamPlayer> getTeamPlayers() {
        return teamPlayers;
    }
}
