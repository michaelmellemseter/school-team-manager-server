package no.experis.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy="user", cascade = CascadeType.REMOVE)
    private Player player;

    @OneToOne(mappedBy="user", cascade = CascadeType.REMOVE)
    private Parent parent;

    @OneToOne(mappedBy="user", cascade = CascadeType.REMOVE)
    private Coach coach;

    @OneToOne(mappedBy="user", cascade = CascadeType.REMOVE)
    private Admin admin;

    @Column(name = "first_name")
    protected String firstName;

    @Column(name = "sur_name")
    protected String surName;

    @Column(name = "email")
    protected String email;

    @Column(name = "mobile_number")
    protected Integer mobileNumber;

    @Column(name = "address")
    protected String address;

    @Column(name = "date")
    protected Date date;

    @ManyToOne
    @JoinColumn(name="role_id")
    public Role role;

    @OneToMany(mappedBy = "senderUser")
    public Set<Notification> senderNotifications;

    @OneToMany(mappedBy = "receiverUser")
    public Set<Notification> receiverNotifications;

    @OneToMany(mappedBy = "user")
    public Set<MessageToAdmin> messagesToAdmins;

    @JsonGetter("messagesToAdmins")
    public List<String> messagesToAdminsGetter() {
        if(messagesToAdmins != null) {
            return messagesToAdmins.stream()
                    .map(messageToAdmins -> {
                        return "/api/v1/admins/messages/" + messageToAdmins.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public User(Long id, String firstName, String surName, String email, Integer mobileNumber, String address, Date date, Role role, Set<MessageToAdmin> messagesToAdmins) {
        this.id = id;
        this.firstName = firstName;
        this.surName = surName;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.address = address;
        this.date = date;
        this.role = role;
        this.messagesToAdmins = messagesToAdmins;
    }

    public User() {

    }

    //getters and setters
    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(Integer mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

}
