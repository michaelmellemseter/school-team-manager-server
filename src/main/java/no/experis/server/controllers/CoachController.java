package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.Coach;
import no.experis.server.models.Match;
import no.experis.server.repositories.CoachRepository;
import no.experis.server.repositories.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@RestController
@RequestMapping(value = "api/v1/coaches")
public class CoachController {


    @Autowired
    private CoachRepository coachRepository;

    @Autowired
    private MatchRepository matchRepository;

    // return all coaches in database.
    @Operation(summary = "Get all coaches from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all coaches",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Coach.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @CrossOrigin
    @GetMapping
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<List<Coach>> getAllCoaches() {
        List<Coach> coaches = coachRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(coaches, resp);
    }

    @Operation(summary = "Get a coach by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the coach",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Coach.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Coach not found",
                    content = @Content)})
    @CrossOrigin
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Coach> getCoachById(@PathVariable Long id) {
        Coach returnCoach;
        HttpStatus resp;

        if (coachRepository.findById(id).isPresent()) {
            System.out.println("Coach with id: " + id);
            returnCoach = coachRepository.findById(id).get();
            resp = HttpStatus.OK;
        } else {
            System.out.println("Coach not found");
            returnCoach = null;
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(returnCoach, resp);
    }

    //Deletes a coach recognized by id from the database.
    @Operation(summary = "Delete a coach by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted the coach",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Coach.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Coach not found",
                    content = @Content)})
    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<HttpStatus> deleteCoach(@PathVariable Long id) {
        HttpStatus status;
        if (coachRepository.findById(id).isPresent()) {
            status = HttpStatus.NO_CONTENT;
            coachRepository.deleteById(id);
            System.out.println("Coach successfully deleted from database");
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }

    @Operation(summary = "Update a coach partially, by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the coach",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Coach.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Coach not found",
                    content = @Content)
    })
    @CrossOrigin
    @PatchMapping("/{id}")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<HttpStatus> updateCoachPartially(@PathVariable Long id, @RequestBody Coach coach) {

        HttpStatus status;

        if(coachRepository.findById(id).isPresent()) {
            Coach updCoach = coachRepository.findById(id).get();
            if (!id.equals(updCoach.getId())) {
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(status);
            }

            if(coach.getUser().getAddress() != null) {
                updCoach.getUser().setAddress(coach.getUser().getAddress());
            }

            if(coach.getUser().getMobileNumber() > 0) {
                updCoach.getUser().setMobileNumber(coach.getUser().getMobileNumber());
            }

            if(coach.getUser().getEmail() != null) {
                updCoach.getUser().setEmail(coach.getUser().getEmail());
            }

            coachRepository.save(updCoach);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }


    @Operation(summary = "Get either previous or upcoming matches for a coach", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned matches",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Match.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "400", description = "invalid request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Coach not found",
                    content = @Content)
    })
    @CrossOrigin
    @GetMapping("/{id}/matches")
    @PreAuthorize("hasRole('Coach')")
    public ResponseEntity<List<Match>> getMatches(@PathVariable Long id,  @RequestParam String time){
        HttpStatus status;
        if(coachRepository.findById(id).isPresent()) {
            List<Match> matches;
            if(time.equalsIgnoreCase("previous")) {
                matches = matchRepository.getPreviousCoachMatches(id);
                status = HttpStatus.OK;
                return new ResponseEntity<>(matches, status);
            }
            else if(time.equalsIgnoreCase("upcoming")) {
                matches = matchRepository.getUpComingCoachMatches(id);
                status = HttpStatus.OK;
                return new ResponseEntity<>(matches, status);
            }
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }


}
