package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.Coach;
import no.experis.server.models.Location;
import no.experis.server.models.LocationType;
import no.experis.server.models.School;
import no.experis.server.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "api/v1/locations")
public class LocationController {

    @Autowired
    private LocationRepository locationRepository;

    @Operation(summary = "Get all locations from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all coaches",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Location.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<List<Location>> getAllLocations() {
        List<Location> locations = locationRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(locations, resp);
    }

    @Operation(summary = "Get a location by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the location",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Location.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Location not found",
                    content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<Location> getLocationById(@Parameter(description = "Id of location to be searched") @PathVariable Long id) {
        Location returnLocation = new Location();
        HttpStatus status;
        if (locationRepository.findById(id).isPresent()) {
            status = HttpStatus.OK;
            returnLocation = locationRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnLocation, status);
    }

    @Operation(summary = "Update a location by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the location",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Location.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Location not found",
                    content = @Content),

    })
    @CrossOrigin
    @PutMapping("/{id}")
    public ResponseEntity<Location> updateLocation(@Parameter(description = "Id of location to be updated") @PathVariable Long id, @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The updated location", required = true, content = @Content(
            schema = @Schema(implementation = Location.class))) @RequestBody Location Location) {
        Location returnLocation = new Location();
        HttpStatus status;
        if (!id.equals(Location.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnLocation, status);
        }
        if(locationRepository.findById(id).isPresent())
        {
            returnLocation = locationRepository.save(Location);
            status = HttpStatus.NO_CONTENT;
        } else
        {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(returnLocation, status);
    }

    @Operation(summary = "Create a location", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created a new location",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Location.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity<Location> addLocation(@io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The new location", required = true, content = @Content(
            schema = @Schema(implementation = Location.class))) @RequestBody Location Location) {
        Location returnLocation = locationRepository.save(Location);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnLocation, status);
    }

    @Operation(summary = "Delete a location by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the location",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Location.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Location not found",
                    content = @Content)

    })
    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    public HttpStatus deleteLocation(@Parameter(description = "Id of location to be deleted") @PathVariable Long id) {
        HttpStatus status;
        if (locationRepository.findById(id).isPresent()) {
            status = HttpStatus.OK;
            Location deleteLocation = locationRepository.findById(id).get();

            Set<School> schools = deleteLocation.schools;
            for (School school : schools) school.locations.remove(deleteLocation);

            Set<LocationType> locationTypes = deleteLocation.locations;
            for (LocationType locationType : locationTypes) locationType.locations.remove(deleteLocation);

            locationRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }
}
