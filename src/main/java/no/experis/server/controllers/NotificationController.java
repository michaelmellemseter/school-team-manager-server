package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.*;
import no.experis.server.models.dto.NotificationDTO;
import no.experis.server.repositories.*;
import no.experis.server.services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/notifications")
public class NotificationController {
    @Autowired
    NotificationRepository notificationRepository;
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    CoachRepository coachRepository;
    @Autowired
    TeamRepository teamRepository;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    SchoolRepository schoolRepository;
    @Autowired
    SportRepository sportRepository;
    @Autowired
    NotificationService notificationService;

    @Operation(summary = "Get all notifications from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all notifications",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Notification.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping()
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<List<Notification>> getAllNotifications(){
        List<Notification> notifications = notificationRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(notifications, status);
    }

    @Operation(summary = "Send notification from coach to parents", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Notification created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Notification.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Coach not found",
                    content = @Content)
    })
    @PostMapping("/player")
    @PreAuthorize("hasRole('Coach')")
    public ResponseEntity<HttpStatus> sendNotificationPlayerParents(@RequestBody NotificationDTO notificationDTO){
        HttpStatus status;
        boolean sent = false;
        if(userRepository.findById(notificationDTO.senderId).isPresent())
        {
            User senderUser = userRepository.findById(notificationDTO.senderId).get();
            if(playerRepository.findById(notificationDTO.receiverId).isPresent()){
                Player player = playerRepository.findById(notificationDTO.receiverId).get();
                if(!player.parents.isEmpty()){
                    for (Parent parent : player.parents){
                        //create a notification for this specific parent
                        Notification notification = new Notification();
                        notification.setMessage(notificationDTO.message);
                        notification.setSeen(false);
                        notification.setSenderUser(senderUser);
                        notification.setReceiverUser(parent.user);
                        notificationRepository.save(notification);
                    }
                    status = HttpStatus.CREATED;
                } else {
                    status = HttpStatus.NOT_FOUND;
                }
                return new ResponseEntity<>(status);
            }
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

    @Operation(summary = "Send notification from admin or coach to parents through team", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Notification created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Notification.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Sender or receiver not found",
                    content = @Content)
    })
    @PostMapping("/team")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<HttpStatus> sendNotificationTeam(@RequestBody NotificationDTO notificationDTO) {
        HttpStatus status;
        boolean sent = false;
        if(userRepository.findById(notificationDTO.senderId).isPresent()){
            User senderUser = userRepository.findById(notificationDTO.senderId).get();
            if (teamRepository.findById(notificationDTO.receiverId).isPresent()) {
                Team team = teamRepository.findById(notificationDTO.receiverId).get();

                if(notificationService.teamParents(senderUser, team, notificationDTO.message)){
                    sent = true;
                }
                if(sent){
                    status = HttpStatus.CREATED;
                } else {
                    status = HttpStatus.NOT_FOUND;
                }
                return new ResponseEntity<>(status);
            }
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

    @Operation(summary = "Send notification from admin to parents through school", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Notification created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Notification.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Sender or receiver not found",
                    content = @Content)
    })
    @PostMapping("/school")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<HttpStatus> sendNotificationSchool(@RequestBody NotificationDTO notificationDTO) {
        HttpStatus status;
        boolean sent = false;
        if(userRepository.findById(notificationDTO.senderId).isPresent()){
            User senderUser = userRepository.findById(notificationDTO.senderId).get();
            if(schoolRepository.findById(notificationDTO.receiverId).isPresent()){
                School school = schoolRepository.findById(notificationDTO.receiverId).get();
                for(Team team : school.teams)
                {
                    if(notificationService.teamParents(senderUser, team, notificationDTO.message)){
                        sent = true;
                    }
                }
                if(sent){
                    status = HttpStatus.CREATED;
                } else {
                    status = HttpStatus.NOT_FOUND;
                }
                return new ResponseEntity<>(status);
            }
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

    @Operation(summary = "Send notification from coach to parents through sport", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Notification created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Notification.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Sender or receiver not found",
                    content = @Content)
    })
    @PostMapping("/sport")
    @PreAuthorize("hasRole('Coach')")
    public ResponseEntity<HttpStatus> addAdminNotificationsSport(@RequestBody NotificationDTO notificationDTO) {
        HttpStatus status;
        boolean sent = false;
        if(userRepository.findById(notificationDTO.senderId).isPresent()){
            User senderUser = userRepository.findById(notificationDTO.senderId).get();
            if(sportRepository.findById(notificationDTO.receiverId).isPresent()){
                Sport sport = sportRepository.findById(notificationDTO.receiverId).get();

                for(Team team : sport.teams)
                {
                    if(notificationService.teamParents(senderUser, team, notificationDTO.message)){
                        sent = true;
                    }
                }
                if(sent){
                    status = HttpStatus.CREATED;
                } else {
                    status = HttpStatus.NOT_FOUND;
                }
                return new ResponseEntity<>(status);
            }
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

    @CrossOrigin
    @PatchMapping("/{id}/seen")
    @PreAuthorize("hasRole('Parent')")
    public ResponseEntity<Notification> updateNotificationToSeen(@Parameter(description = "Id of notification to be seen") @PathVariable Long id) {
        HttpStatus status;
        if(notificationRepository.findById(id).isPresent()) {
            Notification returnNotification = notificationRepository.findById(id).get();
            returnNotification.setSeen(true);
            notificationRepository.save(returnNotification);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(returnNotification, status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
    }
}
