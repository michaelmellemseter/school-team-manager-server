package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.*;
import no.experis.server.models.dto.PlayerDTO;
import no.experis.server.models.dto.TeamPlayerDTO;
import no.experis.server.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "api/v1/teams")
public class TeamController {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private CoachRepository coachRepository;
    @Autowired
    private MatchRepository matchRepository;
    @Autowired
    private TeamPlayerRepository teamPlayerRepository;

    @Operation(summary = "Get all teams from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all teams",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Team.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<List<Team>> getAllTeams() {
        List<Team> teams = teamRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(teams, resp);
    }

    @Operation(summary = "Get a team by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the team",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Team.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Team not found",
                    content = @Content)})
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Team> getTeamById(@Parameter(description = "Id of team to be searched") @PathVariable Long id) {
        Team returnTeam = new Team();
        HttpStatus status;
        if (teamRepository.findById(id).isPresent()) {
            status = HttpStatus.OK;
            returnTeam = teamRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnTeam, status);
    }

    @Operation(summary = "Update a team by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the team",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Team.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Team not found",
                    content = @Content)
    })
    @CrossOrigin
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<Team> updateTeam(@Parameter(description = "Id of team to be updated") @PathVariable Long id, @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The updated team", required = true, content = @Content(
            schema = @Schema(implementation = Team.class))) @RequestBody Team Team) {
        Team returnTeam = new Team();
        HttpStatus status;
        if (!id.equals(Team.getId())) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnTeam, status);
        }
        returnTeam = teamRepository.save(Team);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnTeam, status);
    }

    @Operation(summary = "Create a team", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created a new team",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Team.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @PostMapping
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<Team> addTeam(@io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The new team", required = true, content = @Content(
            schema = @Schema(implementation = Team.class))) @RequestBody Team Team) {
            Team returnTeam = teamRepository.save(Team);
            HttpStatus status = HttpStatus.CREATED;
            return new ResponseEntity<>(returnTeam, status);
    }

    @Operation(summary = "Delete a team by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the team",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Team.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Team not found",
                    content = @Content)})
    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public HttpStatus deleteTeam(@Parameter(description = "Id of team to be deleted") @PathVariable Long id) {
        HttpStatus status;
        if (teamRepository.findById(id).isPresent()) {
            status = HttpStatus.OK;
            Team deleteTeam = teamRepository.findById(id).get();

            // take care of cascading
            Set<Coach> coaches = deleteTeam.coaches;
            for (Coach coach : coaches) coach.teams.remove(deleteTeam);

            Set<Match> homeMatches = deleteTeam.homeMatches;
            for (Match match : homeMatches) {
                Team awayTeam = match.awayTeam;
                if(awayTeam != null) awayTeam.awayMatches.remove(match);
                matchRepository.deleteById(match.getId());
            }

            Set<Match> awayMatches = deleteTeam.awayMatches;
            for (Match match : awayMatches) {
                Team homeTeam = match.homeTeam;
                if(homeTeam != null) homeTeam.homeMatches.remove(match);
                matchRepository.deleteById(match.getId());
            }

            Set<TeamPlayer> teamPlayers = deleteTeam.getTeamPlayers();
            for(TeamPlayer teamplayer : teamPlayers ){
                teamPlayerRepository.delete(teamplayer);
            }
            teamRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }

    @Operation(summary = "Set players to a given team", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Players added to team",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Team.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Team not found",
                    content = @Content)

    })
    @CrossOrigin
    @PatchMapping("/{id}/players")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<Team> setPlayers(@PathVariable Long id, @RequestBody TeamPlayerDTO teamPlayerDTO){
        HttpStatus status;
        if(teamRepository.findById(id).isPresent()){
            Team team = teamRepository.findById(id).get();
                if(playerRepository.findById(teamPlayerDTO.playerId).isPresent()){
                    Player playerToAdd = playerRepository.findById(teamPlayerDTO.playerId).get();
                    TeamPlayer teamplayer = new TeamPlayer();
                    teamplayer.setTeam(team);
                    teamplayer.setPlayer(playerToAdd);
                    teamplayer.setJerseyNumber(teamPlayerDTO.jerseyNr);
                    teamPlayerRepository.save(teamplayer);
                    status = HttpStatus.NO_CONTENT;
                    return new ResponseEntity<>(status);
            }
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

    @Operation(summary = "Set coaches to a given team", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Coaches added to team",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Team.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Team not found",
                    content = @Content)

    })
    @CrossOrigin
    @PatchMapping("/{id}/coaches")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Team> setCoaches(@PathVariable Long id, @RequestBody ArrayList<Long> coachIds){
        HttpStatus status;
        if(teamRepository.findById(id).isPresent()){
            Team team = teamRepository.findById(id).get();
            for(Long coach : coachIds){
                if(coachRepository.findById(coach).isPresent()){
                    Coach coachToAdd = coachRepository.findById(coach).get();
                    team.coaches.add(coachToAdd);
                }
            }
            status = HttpStatus.NO_CONTENT;
            teamRepository.save(team);
            return new ResponseEntity<>(team, status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
    }

    @Operation(summary = "Get players for the given team", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all teams",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Player.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Team not found",
                    content = @Content)

    })
    @CrossOrigin
    @GetMapping("/{id}/players")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<Set<PlayerDTO>> getPlayersByTeamId(@PathVariable Long id) {
        Set<PlayerDTO> players = new HashSet<>();
        HttpStatus resp;
        if(teamRepository.findById(id).isPresent()){
            System.out.println("found team by id: " +id);
            resp = HttpStatus.OK;
            Team team = teamRepository.findById(id).get();

            Set<TeamPlayer> teamPlayers = team.getTeamPlayers();
            for(TeamPlayer teamPlayer : teamPlayers){
                PlayerDTO playerDTO = new PlayerDTO(teamPlayer.player, teamPlayer.jerseyNumber);
                players.add(playerDTO);
            }
        }else {
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(players, resp);
    }

    @Operation(summary = "Delete player on  given team", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted player from team",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Team.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Team not found",
                    content = @Content)

    })
    @CrossOrigin
    @DeleteMapping(value = "/{id}/players/{playerId}")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<HttpStatus> deletePlayerByTeam(@PathVariable Long id, @PathVariable Long playerId){
        if(teamRepository.findById(id).isPresent()){
            Team team = teamRepository.findById(id).get();
            Set<TeamPlayer> players = team.getTeamPlayers();
            for (TeamPlayer player : players){
                if(player.player.getId().equals(playerId))
                {
                    teamPlayerRepository.delete(player);
                }
            }
            HttpStatus status = HttpStatus.NO_CONTENT;
            return  new ResponseEntity<>(status);
        }
        HttpStatus status = HttpStatus.NOT_FOUND;
        return  new ResponseEntity<>(status);
    }

}

